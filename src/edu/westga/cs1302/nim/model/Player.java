package edu.westga.cs1302.nim.model;

import edu.westga.cs1302.nim.errormessages.ErrorMessages;

/**
 * This class defines the player
 * 
 * @author Alex DeCesare
 * @version 10-July-2020
 */

public abstract class Player {
	
	private static final int MINIMUM_STICKS_TO_TAKE = 1;

	private String name;
	private Pile thePile;
	private int sticksToTake;
	
	/**
	 * The constructor for the player
	 * 
	 * @precondition name != null && name.isEmpty() == false
	 * @postcondition name = getName && sticks = getSticks
	 * @param name the name of the player
	 * @param thePileOfSticks the amount of sticks the player has
	 */
	
	public Player(String name, Pile thePileOfSticks) {
		
		if (name == null) {
			throw new IllegalArgumentException(ErrorMessages.THE_PLAYER_NAME_CANNOT_BE_NULL);
		}
		if (name.isEmpty()) {
			throw new IllegalArgumentException(ErrorMessages.THE_PLAYER_NAME_CANNOT_BE_EMPTY);
		}
		
		this.name = name;
		this.thePile = thePileOfSticks;

	}
	
	/**
	 * Gets the player name
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the name
	 */
	
	public String getName() {
		return this.name;
	}
	
	/**
	 * Gets the pile
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the pile
	 */
	
	public Pile getPile() {
		return this.thePile;
	}
	
	/**
	 * Gets the sticks for the current turn
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the number of sticks for the current turn
	 */
	
	public int getSticksToTake() {
		return this.sticksToTake;
	}
	
	/**
	 * Sets the pile used for the current turn
	 * 
	 * @precondition pileToSet != null
	 * @postcondition thePile = pileToSet
	 * 
	 * @param pileToSet the sticks to set the pile to
	 */
	
	public void setCurrentPile(Pile pileToSet) {
		
		if (pileToSet == null) {
			throw new IllegalArgumentException(ErrorMessages.CANNOT_SET_A_NULL_PILE);
		}
		
		this.thePile = pileToSet;
	}
	
	/**
	 * Sets the number of sticks used
	 * 
	 * @precondition sticksToSet >= 1
	 * @postcondition sticksToTake = sticksToSet
	 * 
	 * @param sticksToSet the sticks to set the pile to
	 */
	
	public void setSticksForTurn(int sticksToSet) {
		
		if (sticksToSet < MINIMUM_STICKS_TO_TAKE) {
			throw new IllegalArgumentException(ErrorMessages.CANNOT_SET_STICKS_FOR_TURN_AT_LESS_THAN_MINIMUM);
		}
		
		this.sticksToTake = sticksToSet;
	}
	
	/**
	 * Takes the next turn by removing sticks from the pile
	 * 
	 * @precondition none
	 * @postcondition getPile().getSticksLeft = getPile().getSticksLeft - getSticksToTake
	 */
	
	public void takeTurn() {
		
		this.setSticksToTake();
		this.getPile().removeSticks(this.getSticksToTake());
		
	}
	
	/**
	 * sets the default amount of sticks to take
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	abstract void setSticksToTake();
}
