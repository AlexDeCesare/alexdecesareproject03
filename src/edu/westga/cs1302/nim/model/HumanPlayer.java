package edu.westga.cs1302.nim.model;

/**
 * This class defines the human player
 * 
 * @author Alex DeCesare
 * @version 11-July-2020
 */

public class HumanPlayer extends Player {
	
	private static final int MINIMUM_AMOUNT_OF_STICKS_TO_LEAVE = 1;
	
	/**
	 * The constructor for the human player
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param name the name of the human player
	 * @param thePileOfSticks the sticks the of the human player
	 */
	
	public HumanPlayer(String name, Pile thePileOfSticks) {
		super(name, thePileOfSticks);
	}
	
	/**
	 * Sets the default sticks to take for the human player
	 * 
	 * @precondition none
	 * @postcondition none
	 */

	@Override
	public void setSticksToTake() {
		
		if (super.getSticksToTake() >= this.getPile().getSticksLeft()) {
			super.setSticksForTurn(this.getPile().getSticksLeft() - MINIMUM_AMOUNT_OF_STICKS_TO_LEAVE);
		}
	}
	
	/**
	 * The toString method for the human player
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the string representation of the human player
	 */
	
	@Override
	public String toString() {
		return "A human player with " + super.getSticksToTake() + " sticks to take and a name of " + super.getName();
	}

}
