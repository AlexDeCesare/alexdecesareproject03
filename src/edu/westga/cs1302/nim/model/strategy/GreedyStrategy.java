package edu.westga.cs1302.nim.model.strategy;

import edu.westga.cs1302.nim.errormessages.ErrorMessages;

/**
 * Defines the greedy strategy for the computer player
 * 
 * @author Alex DeCesare
 * @version 12-July-2020
 */

public class GreedyStrategy implements NumberOfSticksStrategy {
	
	private static final int MINIMUM_NUMBER_OF_STICKS_LEFT = 1;
	
	/**
	 * returns the number of sticks to be taken from the pile
	 * 
	 * @precondition numberOfSticks >= MINIMUM_NUMBER_OF_STICKS_FOR_GAME
	 * @postcondition none
	 * 
	 * @param pileSize the size of the pile
	 * 
	 * @return sticksToTake the maximum sticks to take which is size of the pile subtracted one
	 */
	
	@Override
	public int howManySticks(int pileSize) {
		
		if (pileSize <= MINIMUM_NUMBER_OF_STICKS_LEFT) {
			throw new IllegalArgumentException(ErrorMessages.CANNOT_SET_STRATEGY_WITH_STICKS_LESS_THAN_MINIMUM);
		}
		
		int sticksToTake = pileSize - MINIMUM_NUMBER_OF_STICKS_LEFT;
		
		return sticksToTake;
	}
	
	/**
	 * Returns the string representation of the greedy strategy
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the string representation of the greedy strategy
	 */
	
	@Override
	public String toString() {
		return "The greedy strategy";
	}

}
