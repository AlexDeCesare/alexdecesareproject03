package edu.westga.cs1302.nim.model.strategy.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.strategy.GreedyStrategy;

class TestGreedyStrategy {

	@Test
	public void shouldNotReturnForSticksWellLessThanOne() {
		
		GreedyStrategy theGreedyStrategy = new GreedyStrategy();
		
		assertThrows(IllegalArgumentException.class, () -> {
			theGreedyStrategy.howManySticks(-300);
		});
	}
	
	@Test
	public void shouldNotReturnForSticksAtOneLessThanZero() {
		
		GreedyStrategy theGreedyStrategy = new GreedyStrategy();
		
		assertThrows(IllegalArgumentException.class, () -> {
			theGreedyStrategy.howManySticks(-1);
		});
	}
	
	@Test
	public void shouldNotReturnForSticksAtOneBelowMinimum() {
		
		GreedyStrategy theGreedyStrategy = new GreedyStrategy();
		
		assertThrows(IllegalArgumentException.class, () -> {
			theGreedyStrategy.howManySticks(0);
		});
	}
	
	@Test
	public void shouldNotReturnForSticksAtMinimum() {
		
		GreedyStrategy theGreedyStrategy = new GreedyStrategy();
		
		assertThrows(IllegalArgumentException.class, () -> {
			theGreedyStrategy.howManySticks(1);
		});
	}
	
	@Test
	public void shouldReturnOneForSticksOneAboveMinimum() {
		
		GreedyStrategy theGreedyStrategy = new GreedyStrategy();
	
		assertEquals(1, theGreedyStrategy.howManySticks(2));
		
	}
	
	@Test
	public void shouldReturnOneForSticksNearMinimum() {
		
		GreedyStrategy theGreedyStrategy = new GreedyStrategy();
	
		assertEquals(7, theGreedyStrategy.howManySticks(8));
		
	}
	
	@Test
	public void shouldReturnOneForSticksWellAboveMinimum() {
		
		GreedyStrategy theGreedyStrategy = new GreedyStrategy();
	
		assertEquals(299, theGreedyStrategy.howManySticks(300));
		
	}

}
