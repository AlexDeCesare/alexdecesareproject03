package edu.westga.cs1302.nim.model.strategy;

import java.util.Random;

import edu.westga.cs1302.nim.errormessages.ErrorMessages;

/**
 * Defines the random strategy for the computer player
 * 
 * @author Alex DeCesare
 * @version 13-July-2020
 */

public class RandomStrategy implements NumberOfSticksStrategy {
	
	private static final int MINIMUM_NUMBER_OF_STICKS_TO_CONTINUE_GAME = 1;

	/**
	 * returns the random number of sticks to be taken from the pile
	 * 
	 * @precondition numberOfSticks >= MINIMUM_NUMBER_OF_STICKS_FOR_GAME
	 * @postcondition none
	 * 
	 * @param pileSize the size of the pile
	 * 
	 * @return sticksToTake a random amount of sticks between the minimum sticks to take and the size of the pile
	 */
	
	@Override
	public int howManySticks(int pileSize) {

		if (pileSize <= MINIMUM_NUMBER_OF_STICKS_TO_CONTINUE_GAME) {
			throw new IllegalArgumentException(ErrorMessages.CANNOT_SET_STRATEGY_WITH_STICKS_LESS_THAN_MINIMUM);
		}
		
		Random randomNumber = new Random();
		return randomNumber.nextInt(((pileSize - MINIMUM_NUMBER_OF_STICKS_TO_CONTINUE_GAME))) + MINIMUM_NUMBER_OF_STICKS_TO_CONTINUE_GAME;
		
	}
	
	/**
	 * Returns the string representation of the greedy strategy
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the string representation of the greedy strategy
	 */
	
	@Override
	public String toString() {
		return "The random strategy";
	}

}
