package edu.westga.cs1302.nim.view.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.Game;
import edu.westga.cs1302.nim.view.View;

class TestGetDescription {

	@Test
	public void shouldNotGetDescriptionOfNullGame() {
		
		View theView = new View();
		
		assertThrows(IllegalArgumentException.class, () -> {
			
			theView.getGameDescription(null);
			
		});
		
	}
	
	@Test
	public void shouldGetDescriptionOfDefaultGameInProgress() {
		
		View theView = new View();
		Game theGame = new Game();
		
		assertEquals("Game still in progress the number of sticks left is: 7", theView.getGameDescription(theGame));
		
	}
	
	@Test
	public void shouldGetDescriptionOfCompletedGame() {
		
		View theView = new View();
		Game theGame = new Game();
		
		theGame.getHumanPlayer().setSticksForTurn(6);
		theGame.play();
		
		assertEquals("The winner of the game is Me", theView.getGameDescription(theGame));
		
	}

}
