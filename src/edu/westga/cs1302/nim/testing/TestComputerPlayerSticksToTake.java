package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.ComputerPlayer;
import edu.westga.cs1302.nim.model.Pile;
import edu.westga.cs1302.nim.model.strategy.CautiousStrategy;
import edu.westga.cs1302.nim.model.strategy.GreedyStrategy;
import edu.westga.cs1302.nim.model.strategy.NumberOfSticksStrategy;

class TestComputerPlayerSticksToTake {

	@Test
	public void shouldNotTakeAnySticksIfTheTotalSticksIsOne() {
		
		Pile testPile = new Pile(1);
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
			
		ComputerPlayer testComputerPlayer = new ComputerPlayer("the Computer player", testPile);
		testComputerPlayer.setSticksStrategy(theStrategy);
		
		assertThrows(IllegalArgumentException.class, () -> {
			testComputerPlayer.setSticksToTake();
		});
		
	}
	
	@Test 
	public void shouldTakeOneStickIfTotalSticksIsThreeWithCautiousStrategy() {
		
		Pile testPile = new Pile(3);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("the Computer player", testPile);
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		testComputerPlayer.setSticksStrategy(theStrategy);
		testComputerPlayer.setSticksToTake();
		
		assertEquals(1, testComputerPlayer.getSticksToTake());
	}
	
	@Test 
	public void shouldTakeThreeSticksIfTotalSticksIsFourWithCautiousStrategy() {
		
		Pile testPile = new Pile(4);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("the Computer player", testPile);
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		testComputerPlayer.setSticksStrategy(theStrategy);
		testComputerPlayer.setSticksToTake();
		
		assertEquals(1, testComputerPlayer.getSticksToTake());
	}
	
	@Test 
	public void shouldTakeThreeSticksIfTotalSticksIsWellAboveFourWithCautiousStrategy() {
		
		Pile testPile = new Pile(400);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("the Computer player", testPile);
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		testComputerPlayer.setSticksStrategy(theStrategy);
		testComputerPlayer.setSticksToTake();
		
		assertEquals(1, testComputerPlayer.getSticksToTake());
	}
	
	@Test 
	public void shouldTakeOneStickIfTotalSticksIsThreeWithGreedyStrategy() {
		
		Pile testPile = new Pile(3);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("the Computer player", testPile);
		NumberOfSticksStrategy theStrategy = new GreedyStrategy();
		
		testComputerPlayer.setSticksStrategy(theStrategy);
		testComputerPlayer.setSticksToTake();
		
		assertEquals(2, testComputerPlayer.getSticksToTake());
	}
	
	@Test 
	public void shouldTakeThreeSticksIfTotalSticksIsFourWithGreedyStrategy() {
		
		Pile testPile = new Pile(4);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("the Computer player", testPile);
		NumberOfSticksStrategy theStrategy = new GreedyStrategy();
		
		testComputerPlayer.setSticksStrategy(theStrategy);
		testComputerPlayer.setSticksToTake();
		
		assertEquals(3, testComputerPlayer.getSticksToTake());
	}
	
	@Test 
	public void shouldTakeThreeSticksIfTotalSticksIsWellAboveFourWithGreedyStrategy() {
		
		Pile testPile = new Pile(400);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("the Computer player", testPile);
		NumberOfSticksStrategy theStrategy = new GreedyStrategy();
		
		testComputerPlayer.setSticksStrategy(theStrategy);
		testComputerPlayer.setSticksToTake();
		
		assertEquals(399, testComputerPlayer.getSticksToTake());
	}

}
