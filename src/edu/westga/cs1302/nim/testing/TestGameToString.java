package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.Game;
import edu.westga.cs1302.nim.model.Pile;
import edu.westga.cs1302.nim.model.strategy.CautiousStrategy;
import edu.westga.cs1302.nim.model.strategy.NumberOfSticksStrategy;

class TestGameToString {

	@Test
	public void shouldReturnSticksLeftForGameStartWithDefaultValues() {
		
		Game theGame = new Game();
		
		assertEquals("Game still in progress the number of sticks left is: 7", theGame.toString());
	}
	
	@Test
	public void shouldReturnSticksLeftForGameStartWithValuesSet() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(10);
		
		theGame.setPileForThisTurn(thePile);
		
		assertEquals("Game still in progress the number of sticks left is: 10", theGame.toString());
	}
	
	@Test
	public void shouldReturnSticksLeftAfterOneTurn() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(10);
		
		theGame.setPileForThisTurn(thePile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		theGame.getComputerPlayer().setSticksStrategy(theStrategy);
		
		theGame.getHumanPlayer().setSticksForTurn(3);
		
		theGame.play();
		
		assertEquals("Game still in progress the number of sticks left is: 7", theGame.toString());
	}
	
	@Test
	public void shouldReturnSticksLeftAfterTwoTurns() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(10);
		
		theGame.setPileForThisTurn(thePile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		theGame.getComputerPlayer().setSticksStrategy(theStrategy);
		
		theGame.getComputerPlayer().setSticksToTake();
		theGame.getHumanPlayer().setSticksToTake();
		
		theGame.getHumanPlayer().setSticksForTurn(3);
		
		theGame.play();
		theGame.play();
		
		assertEquals("Game still in progress the number of sticks left is: 6", theGame.toString());
	}
	
	@Test
	public void shouldReturnSticksLeftAfterThreeTurns() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(10);
		
		theGame.setPileForThisTurn(thePile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		theGame.getComputerPlayer().setSticksStrategy(theStrategy);
		
		theGame.getComputerPlayer().setSticksToTake();
		theGame.getHumanPlayer().setSticksToTake();
		theGame.getHumanPlayer().setSticksForTurn(3);
		
		theGame.play();
		theGame.play();
		theGame.play();
		
		assertEquals("Game still in progress the number of sticks left is: 3", theGame.toString());
	}
	
	@Test
	public void shouldReturnSticksLeftAfterFourTurns() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(11);
		
		theGame.setPileForThisTurn(thePile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		theGame.getComputerPlayer().setSticksStrategy(theStrategy);
		
		theGame.getComputerPlayer().setSticksToTake();
		theGame.getHumanPlayer().setSticksToTake();
		
		theGame.getHumanPlayer().setSticksForTurn(3);
		
		theGame.play();
		theGame.play();
		theGame.play();
		theGame.play();
		
		assertEquals("Game still in progress the number of sticks left is: 3", theGame.toString());
	}
	
	@Test
	public void shouldReturnGameWinnerAtOneStickLeft() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(3);
		
		theGame.setPileForThisTurn(thePile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		theGame.getComputerPlayer().setSticksStrategy(theStrategy);
		theGame.getHumanPlayer().setSticksForTurn(3);
		
		theGame.play();
		
		assertEquals("The winner of the game is Me", theGame.toString());
		
	}

}
