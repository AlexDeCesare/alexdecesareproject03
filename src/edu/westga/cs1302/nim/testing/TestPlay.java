package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.Game;
import edu.westga.cs1302.nim.model.Pile;
import edu.westga.cs1302.nim.model.strategy.CautiousStrategy;
import edu.westga.cs1302.nim.model.strategy.NumberOfSticksStrategy;

class TestPlay {

	@Test
	public void shouldPlayGameAtOneAboveMinimumSticksToPlay() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(3);
		
		theGame.setPileForThisTurn(thePile);
		
		theGame.getHumanPlayer().setSticksForTurn(3);
		
		theGame.play();
		
		assertEquals("The winner of the game is Me", theGame.toString());
		
	}
	
	@Test
	public void shouldPlayGameWhenMeWinsAboveMinimumSticksToPlay() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(4);
		
		theGame.setPileForThisTurn(thePile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		theGame.getComputerPlayer().setSticksStrategy(theStrategy);
		
		theGame.getHumanPlayer().setSticksForTurn(3);
		
		theGame.play();
		
		assertEquals("The winner of the game is Me", theGame.toString());
		
	}
	
	@Test
	public void shouldPlayGameWhenComputerWinsAboveMinimumSticksToPlay() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(5);
		
		theGame.setPileForThisTurn(thePile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		theGame.getComputerPlayer().setSticksStrategy(theStrategy);
		
		theGame.getHumanPlayer().setSticksForTurn(3);
		
		theGame.play();
		theGame.play();
		
		assertEquals("The winner of the game is Computer", theGame.toString());
		
	}
	
	@Test
	public void shouldPlayGameOneTimeWhenSticksLeftIsNearAboveMinimumSticksToPlay() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(700);
		
		theGame.setPileForThisTurn(thePile);
		
		theGame.getHumanPlayer().setSticksForTurn(3);
		
		theGame.play();
		
		assertEquals("Game still in progress the number of sticks left is: 697", theGame.toString());
		
	}
	
	@Test
	public void shouldPlayGameMultipleTimesWhenSticksLeftIsNearAboveMinimumSticksToPlay() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(7);
		
		theGame.setPileForThisTurn(thePile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		theGame.getComputerPlayer().setSticksStrategy(theStrategy);
		
		theGame.getHumanPlayer().setSticksForTurn(3);
		
		theGame.play();
		theGame.play();
		
		assertEquals("Game still in progress the number of sticks left is: 3", theGame.toString());
		
	}
	
	@Test
	public void shouldPlayGameMultipleTimesWhenSticksLeftIsWellAboveMinimumSticksToPlay() {
		
		Game theGame = new Game();
		Pile thePile = new Pile(700);
		
		theGame.setPileForThisTurn(thePile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		theGame.getComputerPlayer().setSticksStrategy(theStrategy);
		
		theGame.getHumanPlayer().setSticksForTurn(3);
		
		theGame.play();
		theGame.play();
		
		assertEquals("Game still in progress the number of sticks left is: 696", theGame.toString());
		
	}

}
