package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.Pile;

class TestGetSticksLeft {

	@Test
	public void shouldGetSticksLeftWellAboveZeroWithoutRemoving() {
		
		Pile testPile = new Pile(500);
		assertEquals(500, testPile.getSticksLeft());
		
	}
	
	@Test
	public void shouldGetSticksLeftOneAboveZeroWithoutRemoving() {
		
		Pile testPile = new Pile(1);
		assertEquals(1, testPile.getSticksLeft());
		
	}
	
	@Test
	public void shouldGetSticksLeftWithWellAboveZeroRemoving() {
		
		Pile testPile = new Pile(500);
		testPile.removeSticks(1);
		assertEquals(499, testPile.getSticksLeft());
		
	}
	
	@Test
	public void shouldGetSticksLeftWithOneAboveMinimumNumberForWinningRemoving() {
		
		Pile testPile = new Pile(500);
		testPile.removeSticks(1);
		assertEquals(499, testPile.getSticksLeft());
		
	}
	
	@Test
	public void shouldGetSticksLeftWithOneBelowMaximumNumberToRemove() {
		
		Pile testPile = new Pile(500);
		testPile.removeSticks(2);
		assertEquals(498, testPile.getSticksLeft());
		
	}
	
	@Test
	public void shouldGetSticksLeftWithMaximumNumberToRemove() {
		
		Pile testPile = new Pile(500);
		testPile.removeSticks(3);
		assertEquals(497, testPile.getSticksLeft());
		
	}

}
