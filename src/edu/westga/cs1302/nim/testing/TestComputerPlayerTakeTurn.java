package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.ComputerPlayer;
import edu.westga.cs1302.nim.model.Pile;
import edu.westga.cs1302.nim.model.strategy.CautiousStrategy;
import edu.westga.cs1302.nim.model.strategy.NumberOfSticksStrategy;

class TestComputerPlayerTakeTurn {

	@Test
	public void shouldNotTakeTurnWhenSticksLeftIsAtMinimum() {
		
		Pile testPile = new Pile(1);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("The Player", testPile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		testComputerPlayer.setSticksStrategy(theStrategy);

		assertThrows(IllegalArgumentException.class, () -> {
		
			testComputerPlayer.takeTurn();
		
		});
	}
	
	@Test
	public void shouldNotTakeTurnWhenSticksLeftIsOneBelowMinimum() {
		
		Pile testPile = new Pile(1);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("The Player", testPile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		testComputerPlayer.setSticksStrategy(theStrategy);

		assertThrows(IllegalArgumentException.class, () -> {
		
			testComputerPlayer.takeTurn();
		
		});
	}
	
	@Test
	public void shouldTakeTurnWhenSticksLeftIsWellAboveMinimum() {
		
		Pile testPile = new Pile(20);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("The Player", testPile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		testComputerPlayer.setSticksStrategy(theStrategy);

		testComputerPlayer.takeTurn();
		
		assertEquals(19, testComputerPlayer.getPile().getSticksLeft());
	}
	
	@Test
	public void shouldTakeTurnWhenSticksLeftIsOneAboveMaximumThatCanBeTaken() {
		
		Pile testPile = new Pile(4);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("The Player", testPile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		testComputerPlayer.setSticksStrategy(theStrategy);

		testComputerPlayer.takeTurn();
		
		assertEquals(3, testComputerPlayer.getPile().getSticksLeft());
	}
	
	@Test
	public void shouldTakeTurnWhenSticksLeftIsAtMaximumThatCanBeTaken() {
		
		Pile testPile = new Pile(3);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("The Player", testPile);

		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		testComputerPlayer.setSticksStrategy(theStrategy);
		
		testComputerPlayer.takeTurn();
		
		assertEquals(2, testComputerPlayer.getPile().getSticksLeft());
	}
	
	@Test
	public void shouldTakeTurnWhenSticksLeftIsAtMinimumThatCanBeTaken() {
		
		Pile testPile = new Pile(3);
		
		ComputerPlayer testComputerPlayer = new ComputerPlayer("The Player", testPile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		testComputerPlayer.setSticksStrategy(theStrategy);

		testComputerPlayer.takeTurn();
		
		assertEquals(2, testComputerPlayer.getPile().getSticksLeft());
	}

}
