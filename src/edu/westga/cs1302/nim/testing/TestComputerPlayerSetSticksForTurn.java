package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.ComputerPlayer;
import edu.westga.cs1302.nim.model.Pile;
import edu.westga.cs1302.nim.model.Player;

class TestComputerPlayerSetSticksForTurn {

	@Test
	public void cannotSetSticksForTurnAtWellLessThanOne() {
		
		Pile thePile = new Pile(7);
		Player theComputerPlayer = new ComputerPlayer("Me", thePile);
		
		assertThrows(IllegalArgumentException.class, () -> {
			theComputerPlayer.setSticksForTurn(-500);
		});
		
	}
	
	@Test
	public void cannotSetSticksForTurnAtOneLessThanZero() {
		
		Pile thePile = new Pile(7);
		Player theComputerPlayer = new ComputerPlayer("Me", thePile);
		
		assertThrows(IllegalArgumentException.class, () -> {
			theComputerPlayer.setSticksForTurn(-1);
		});
		
	}
	
	@Test
	public void cannotSetSticksForTurnAtZero() {
		
		Pile thePile = new Pile(7);
		Player theComputerPlayer = new ComputerPlayer("Me", thePile);
		
		assertThrows(IllegalArgumentException.class, () -> {
			theComputerPlayer.setSticksForTurn(0);
		});
		
	}
	
	@Test
	public void shouldSetSticksForTurnAtOne() {
		
		Pile thePile = new Pile(7);
		Player theComputerPlayer = new ComputerPlayer("Me", thePile);
		
		theComputerPlayer.setSticksForTurn(1);
	}
	
	@Test
	public void shouldSetSticksForTurnAtOneMoreThanOne() {
		
		Pile thePile = new Pile(7);
		Player theComputerPlayer = new ComputerPlayer("Me", thePile);

		theComputerPlayer.setSticksForTurn(2);
		
		assertEquals(2, theComputerPlayer.getSticksToTake());
		
	}
	
	@Test
	public void shouldSetSticksForTurnAtWellMoreThanOne() {
		
		Pile thePile = new Pile(7);
		Player theComputerPlayer = new ComputerPlayer("Me", thePile);

		theComputerPlayer.setSticksForTurn(500);
		
		assertEquals(500, theComputerPlayer.getSticksToTake());
		
	}

}
