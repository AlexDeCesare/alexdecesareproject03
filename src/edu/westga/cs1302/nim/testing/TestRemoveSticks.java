package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.Pile;

class TestRemoveSticks {

	@Test
	public void shouldNotRemoveSticksWellNegative() {
		
		Pile testPile = new Pile(500);
		
		assertThrows(IllegalArgumentException.class, () -> {
			testPile.removeSticks(-100);
		});
	}
	
	@Test
	public void shouldNotRemoveSticksOneBelowZero() {
		
		Pile testPile = new Pile(500);
		
		assertThrows(IllegalArgumentException.class, () -> {
			testPile.removeSticks(-1);
		});
	}
	
	@Test
	public void shouldNotRemoveZeroSticks() {
		
		Pile testPile = new Pile(500);
		
		assertThrows(IllegalArgumentException.class, () -> {
			testPile.removeSticks(0);
		});
	}
	
	@Test
	public void shouldNotRemoveSticksSoTotalSticksIsWellBelowZero() {
		
		Pile testPile = new Pile(1);
		
		assertThrows(IllegalArgumentException.class, () -> {
			testPile.removeSticks(3);
		});
	}
	
	@Test
	public void shouldNotRemoveSticksSoTotalSticksIsOneBelowZero() {
		
		Pile testPile = new Pile(2);
		
		assertThrows(IllegalArgumentException.class, () -> {
			testPile.removeSticks(3);
		});
	}

	@Test
	public void shouldRemoveOneStick() {
		
		Pile testPile = new Pile(10);
		testPile.removeSticks(1);
		
		assertEquals("A pile with 9 sticks", testPile.toString());
		
	}
	
	@Test
	public void shouldRemoveSomeSticksSoManySticksAreLeft() {
		
		Pile testPile = new Pile(1000);
		testPile.removeSticks(3);
		
		assertEquals("A pile with 997 sticks", testPile.toString());
		
	}
	
	@Test
	public void shouldRemoveManySticksSoTotalSticksIsZero() {
		
		Pile testPile = new Pile(3);
		testPile.removeSticks(3);
		
		assertEquals("A pile with 0 sticks", testPile.toString());
		
	}
	
	@Test
	public void shouldRemoveManySticksSoTotalSticksIsOneAboveZero() {
		
		Pile testPile = new Pile(3);
		testPile.removeSticks(2);
		
		assertEquals("A pile with 1 sticks", testPile.toString());
		
	}

}
