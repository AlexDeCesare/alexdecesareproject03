package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.HumanPlayer;
import edu.westga.cs1302.nim.model.Pile;

class TestHumanPlayerConstructor {

	@Test
	public void shouldNotAddHumanPlayerWithNullName() {
		
		Pile testPile = new Pile(7);
		
		assertThrows(IllegalArgumentException.class, () -> {
			new HumanPlayer(null, testPile);
		});
		
	}
	
	@Test
	public void shouldNotAddHumanPlayerWithEmptyName() {
		
		Pile testPile = new Pile(7);
		
		assertThrows(IllegalArgumentException.class, () -> {
			new HumanPlayer("", testPile);
		});
		
	}
	
	@Test
	public void shouldNotAddHumanPlayerWithSticksWellNegative() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			Pile testPile = new Pile(-500);
			new HumanPlayer("human player", testPile);
		});
		
	}
	
	@Test
	public void shouldNotAddHumanPlayerWithSticksOneBelowZero() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			Pile testPile = new Pile(-1);
			new HumanPlayer("human player", testPile);
		});
		
	}
	
	@Test
	public void shouldNotAddHumanPlayerWithSticksAtZero() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			Pile testPile = new Pile(0);
			new HumanPlayer("human player", testPile);
		});
		
	}
	
	@Test
	public void shouldAddHumanPlayerWithLowerCaseNameOneWord() {
		
		Pile testPile = new Pile(7);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("human", testPile);
		
		assertEquals("human", testHumanPlayer.getName());
		
	}
	
	@Test
	public void shouldAddHumanPlayerWithLowerCaseNameMultipleWords() {
		
		Pile testPile = new Pile(7);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("the human player", testPile);
		
		assertEquals("the human player", testHumanPlayer.getName());
		
	}
	
	@Test
	public void shouldAddHumanPlayerWithUpperCaseNameOneWord() {
		
		Pile testPile = new Pile(7);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("HUMAN", testPile);
		
		assertEquals("HUMAN", testHumanPlayer.getName());
		
	}
	
	@Test
	public void shouldAddHumanPlayerWithUpperCaseNameMultipleWords() {
		
		Pile testPile = new Pile(7);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("THE HUMAN PLAYER", testPile);
		
		assertEquals("THE HUMAN PLAYER", testHumanPlayer.getName());
		
	}
	
	@Test
	public void shouldAddHumanPlayerWithMultipleCaseNameOneWord() {
		
		Pile testPile = new Pile(7);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("Human", testPile);
		
		assertEquals("Human", testHumanPlayer.getName());
		
	}
	
	@Test
	public void shouldAddHumanPlayerWithMultipleCaseNameMultipleWords() {
		
		Pile testPile = new Pile(7);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("The Human Player", testPile);
		
		assertEquals("The Human Player", testHumanPlayer.getName());
		
	}
	
	@Test
	public void shouldAddHumanPlayerWithSticksAtOne() {
		
		Pile testPile = new Pile(1);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("Human Player", testPile);
		
		assertEquals(1, testHumanPlayer.getPile().getSticksLeft());
	}
	
	@Test
	public void shouldAddHumanPlayerWithSticksWellAboveOne() {
		
		Pile testPile = new Pile(500);
		
		HumanPlayer testHumanPlayer = new HumanPlayer("Human Player", testPile);
		
		assertEquals(500, testHumanPlayer.getPile().getSticksLeft());
	}

}
