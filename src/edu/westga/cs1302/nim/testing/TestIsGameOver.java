package edu.westga.cs1302.nim.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.nim.model.Game;
import edu.westga.cs1302.nim.model.Pile;
import edu.westga.cs1302.nim.model.strategy.CautiousStrategy;
import edu.westga.cs1302.nim.model.strategy.NumberOfSticksStrategy;

class TestIsGameOver {

	@Test
	public void shouldReturnTrueIfNumberOfSticksIsTwo() {
		
		Game testGame = new Game();
		Pile testPile = new Pile(1);
		
		testGame.setPileForThisTurn(testPile);
		
		assertEquals(true, testGame.isGameOver());
		
	}
	
	@Test
	public void shouldReturnTrueAfterAllOfTheSticksWereTaken() {
		
		Game testGame = new Game();
		Pile testPile = new Pile(4);
		
		testGame.setPileForThisTurn(testPile);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		theStrategy.howManySticks(testPile.getSticksLeft());
		
		testGame.getComputerPlayer().setSticksStrategy(theStrategy);
		
		testGame.getComputerPlayer().setSticksToTake();
		testGame.getHumanPlayer().setSticksToTake();
		
		testGame.getCurrentPlayer().setSticksForTurn(3);
		testGame.getCurrentPlayer().takeTurn();
		
		assertEquals(true, testGame.isGameOver());
		
	}
	
	@Test
	public void shouldReturnTrueAfterSomeOfTheSticksWereTaken() {
		
		Game testGame = new Game();
		Pile testPile = new Pile(10);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		testGame.getComputerPlayer().setSticksStrategy(theStrategy);
		
		testGame.getComputerPlayer().setSticksToTake();
		testGame.getHumanPlayer().setSticksToTake();
		testGame.getHumanPlayer().setSticksForTurn(3);
		
		testGame.setPileForThisTurn(testPile);
		testGame.getCurrentPlayer().takeTurn();
		
		assertEquals(false, testGame.isGameOver());
		
	}
	
	@Test
	public void shouldReturnTrueAfterTurnsWereSwappedAndNoSticksRemain() {
		
		Game testGame = new Game();
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		testGame.getComputerPlayer().setSticksStrategy(theStrategy);
		
		testGame.getComputerPlayer().setSticksToTake();
		testGame.getHumanPlayer().setSticksToTake();
		testGame.getHumanPlayer().setSticksForTurn(3);
		
		testGame.play();
		testGame.play();
		testGame.play();
		
		assertEquals(true, testGame.isGameOver());
		
	}
	
	@Test
	public void shouldReturnTrueAfterTurnsWereSwappedAndSomeSticksRemain() {
		
		Game testGame = new Game();
		Pile testPile = new Pile(10);
		
		NumberOfSticksStrategy theStrategy = new CautiousStrategy();
		
		testGame.getComputerPlayer().setSticksStrategy(theStrategy);
		
		testGame.getComputerPlayer().setSticksToTake();
		testGame.getHumanPlayer().setSticksToTake();
		testGame.getHumanPlayer().setSticksForTurn(3);
		
		testGame.setPileForThisTurn(testPile);
		
		testGame.play();
		testGame.play();
		testGame.play();
		
		assertEquals(false, testGame.isGameOver());
		
	}
	
	@Test
	public void shouldReturnFalseIfNumberOfSticksOneMoreThanMinimum() {
		
		Game testGame = new Game();
		Pile testPile = new Pile(3);
		
		testGame.setPileForThisTurn(testPile);
		
		assertEquals(false, testGame.isGameOver());
		
	}
	
	@Test
	public void shouldReturnFalseIfNumberOfSticksWellMoreThanMinimum() {
		
		Game testGame = new Game();
		Pile testPile = new Pile(200);
		
		testGame.setPileForThisTurn(testPile);
		
		assertEquals(false, testGame.isGameOver());
		
	}

}
