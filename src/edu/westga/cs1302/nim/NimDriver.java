package edu.westga.cs1302.nim;

import edu.westga.cs1302.nim.controllers.GuiController;

/**
 * This is the driver class for the Nim application
 * 
 * @author	Alex DeCesare
 * @version	10-July-2020
 *
 */
public class NimDriver {

	/**
	 * Entry-point into the application
	 * 
	 * @param args	Not used
	 */
	public static void main(String[] args) {
		GuiController.show();
	}
}
