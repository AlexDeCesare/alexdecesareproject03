package edu.westga.cs1302.nim.controllers;

import edu.westga.cs1302.nim.errormessages.ErrorMessages;
import edu.westga.cs1302.nim.model.Game;
import edu.westga.cs1302.nim.model.strategy.NumberOfSticksStrategy;
import edu.westga.cs1302.nim.view.View;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

/**
 * This class defines the controller functions for the 
 * 	Nim application.  Be sure to add the appropriate Javadoc comments so 
 *  that this code passes Checkstyle
 *  
 *  @author Alex DeCesare
 *  @version 10-July-2020
 *
 */
public class NimController {

	private Game theGame;
	private View theView;
	private Alert theAlert;
	
	/**
	 * The constructor for the controller
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	
	public NimController() {
		this.theGame = new Game();
		this.theView = new View();
		this.theAlert = new Alert(AlertType.ERROR);
	}
	
	/**
	 * Takes the turn for the human
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param sticks the sticks to take
	 */
	
	public void humanTakesTurn(int sticks) {
		try {
			if (!this.isGameOver()) {
				this.theGame.getHumanPlayer().setSticksForTurn(sticks);
				this.theGame.play();
			}
		} catch (IllegalArgumentException theIllegalArgumentException) {
			this.theAlert.setContentText(theIllegalArgumentException.getMessage());
			this.theAlert.showAndWait();
		}
	}
	
	/**
	 * Defines the strategy for the computer
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param strategy the sticks to take
	 */

	public void computerTakesTurn(NumberOfSticksStrategy strategy) {
		try {
			if (!this.isGameOver()) {
				this.theGame.getComputerPlayer().setSticksStrategy(strategy);
				this.theGame.play();
			}
		} catch (IllegalArgumentException theIllegalArgumentException) {
			this.theAlert.setContentText(theIllegalArgumentException.getMessage());
			this.theAlert.showAndWait();
		}
	}
	
	/**
	 * Gets the description for the game
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the description of the game
	 */
	
	public String getGameDescription() {
		
		String theDescription = "";
		
		try {
			theDescription = this.theView.getGameDescription(this.theGame);
		} catch (IllegalArgumentException theIllegalArgumentException) {
			this.theAlert.setContentText(theIllegalArgumentException.getMessage());
			this.theAlert.showAndWait();
		}
		
		return theDescription;
	}

	/**
	 * Gets the computer sticks that are taken
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the sticks taken
	 */
	
	public int getComputerSticksTaken() {
		
		int sticksToTake = 0;
		
		try {
			sticksToTake = this.theGame.getComputerPlayer().getSticksToTake();
		} catch (NullPointerException theNullPointerException) {
			this.theAlert.setContentText(ErrorMessages.DEFINE_THE_STICKS_TO_TAKE);
			this.theAlert.showAndWait();
		}
		
		return sticksToTake;
	}

	/**
	 * Checks if the game is over
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true if the game is over and false if it is not
	 */
	
	public boolean isGameOver() {
		return this.theGame.isGameOver();
	}
}
